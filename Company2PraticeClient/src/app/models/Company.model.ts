export class Company {
    nameCompany : string;
    titleCompany : string;
    dateCreated : Date;
    globalID : number;
}
