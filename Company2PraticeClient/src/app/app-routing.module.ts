import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyComponent } from './components/company/company.component';
import { CreateCompanyComponent } from './components/create-company/create-company.component';
import { EditCompanyComponent } from './components/edit-company/edit-company.component';


const routes: Routes = [
  { path: 'company', component: CompanyComponent },
  { path: 'create', component: CreateCompanyComponent },
  { path: 'edit/:globalID', component: EditCompanyComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
