import { Action } from "@ngrx/store";
import * as CompanyActions from "./../action/company.actions";
import { Company } from "../models/Company.model";



// Section 2
export function reducer(companies: Company[] = [], action: CompanyActions.Actions) {
  // Section 3
  switch (action.type) {
    case CompanyActions.GET_LIST_COMPANY:
      return [...companies,action.payload];
    case CompanyActions.DELETE_COMPANY:
      companies = [];
      return [...companies,action.payload];
    
    default:
      return companies;
  }
}


