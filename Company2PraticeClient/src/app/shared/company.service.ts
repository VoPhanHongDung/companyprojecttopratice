import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { Observable } from "rxjs/internal/Observable"
import { Company } from '../models/Company.model';


const httpOptions = {
  headers: new HttpHeaders({
      'Content-Type': 'application/json',
  })
}


@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  httpBase : string = environment.HttpBase;
    constructor(private http : HttpClient) {

    }

    getCompanies():Observable<Company[]>{
      return this.http.get<Company[]>(this.httpBase + 'company/getlistcompany',httpOptions);
    }

    createCompany(company: any) {
      console.log(company,"service");
      return this.http.post<Response>(this.httpBase + 'company/createcompany',company);
    }

    deleteCompany(globalID:any){
      return this.http.delete<Response>(this.httpBase + 'company/deletecompany/'+globalID);
    }

    updateCompamy(globalID:any , company :any){
      return this.http.put<Response>(this.httpBase + 'company/updatecompany/'+globalID,company);
    }



}
