import { Company } from './models/Company.model';

export interface AppState {
  readonly companies: Company[];
}
