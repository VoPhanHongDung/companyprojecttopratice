import { Component, OnInit } from "@angular/core";
import { CompanyService } from "src/app/shared/company.service";
import { Company } from "src/app/models/Company.model";
import {
  Observable,
  Subject,
  asapScheduler,
  pipe,
  of,
  from,
  interval,
  merge,
  fromEvent
} from "rxjs";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/app.state";
import * as CompanyActions from "../../action/company.actions";

@Component({
  selector: "app-company",
  templateUrl: "./company.component.html",
  styleUrls: ["./company.component.css"],
  providers: [CompanyService]
})
export class CompanyComponent implements OnInit {
  companies: Observable<Company[]>;
  listCompany: Company[];
  globalID: any;
  displayedColumns: string[] = [
    "nameCompany",
    "titleCompany",
    "dateCreated",
    "tools"
  ];
  dataSource;

  constructor(
    private companyService: CompanyService,
    private store: Store<AppState>
  ) {
    this.companyService.getCompanies().subscribe(res => {
      (this.listCompany = res),
        store.dispatch(new CompanyActions.GetListCompany(this.listCompany)),
        store.select("companies").subscribe(res => {
          if (res.length > 0) {
            this.dataSource = res[0];
          }
        });
    });
  }

  ngOnInit() {
    this.store.select("companies").subscribe(res => {
      if (res.length > 0) {
        this.dataSource = res[0];
        console.log("hey tao work", this.dataSource);
      }
    });
  }

  onDelete(globalID) {
    if (confirm("Press a button!")) {
      console.log(globalID);
      this.companyService.deleteCompany(globalID).subscribe(res => {
        alert(res),
          this.companyService.getCompanies().subscribe(data => {
            (this.listCompany = data),
              this.store.dispatch(new CompanyActions.DeleteCompany(data));
            this.store.select("companies").subscribe(res => {
              if (res.length > 0) {
                this.dataSource = res[0];
              }
            });
          });
      });
    } else {
      alert("unsuccessful !");
    }
  }
}
