import { Component, OnInit } from '@angular/core';
import { ParamMap } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CompanyService } from 'src/app/shared/company.service';
import { Router } from '@angular/router';
import { Store } from "@ngrx/store";
import { AppState } from "src/app/app.state";
import * as CompanyActions from "../../action/company.actions";
import { Company } from 'src/app/models/Company.model';




@Component({
  selector: 'app-edit-company',
  templateUrl: './edit-company.component.html',
  styleUrls: ['./edit-company.component.css']
})


export class EditCompanyComponent implements OnInit {
  globalID : any;

  constructor(private route : ActivatedRoute ,  private companyService : CompanyService ,private router : Router ,private store: Store<AppState> ) {
    this.route.paramMap.subscribe((params: ParamMap) =>
    {this.globalID =  params.get('globalID');
      console.log(this.globalID);
    });
  }

  ngOnInit() {

  }

  onClickRegister(createCompanyForm){
    let com = {
      nameCompany : createCompanyForm.value.nameCompany,
      titleCompany : createCompanyForm.value.titleCompany
    };

    this.companyService.updateCompamy(this.globalID,com).subscribe(
      data => {
        console.log("Edit request is successful ", data);
        this.companyService.getCompanies().subscribe(res => {
          this.store.dispatch(new CompanyActions.DeleteCompany(res)),
          this.router.navigate(['\company']);

        });

      },
      error => {
        console.log("Error", error);
      });

  }
}
