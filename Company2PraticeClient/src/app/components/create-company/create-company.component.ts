import { Component, OnInit } from '@angular/core';
import { CompanyService } from 'src/app/shared/company.service';
import { Router } from '@angular/router';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { Store } from "@ngrx/store";
import { AppState } from "src/app/app.state";
import * as CompanyActions from "../../action/company.actions";




@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.css'],
  providers:[CompanyService],
})

export class CreateCompanyComponent implements OnInit {


  constructor(private companyService : CompanyService ,private router : Router, private store: Store<AppState>) { }


  ngOnInit() {
  }

  onClickRegister(createCompanyForm){

    let com = {
      nameCompany : createCompanyForm.value.nameCompany,
      titleCompany : createCompanyForm.value.titleCompany
    };

    this.companyService.createCompany(com).subscribe(
      data => {
        console.log("POST Request is successful ");
        this.companyService.getCompanies().subscribe(res => {

          console.log("tao chay roi");
          this.store.dispatch(new CompanyActions.DeleteCompany(res)),
          this.router.navigate(['\company']);

        });
        
        
      },
      error => {
        console.log("Error", error);
      });

      

  }


}


