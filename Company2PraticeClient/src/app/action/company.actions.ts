// Section 1
//Here, we're simply importing our Tutorial model and Action from ngrx/store. This makse sense, being that we're working with actions.
import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";
import { Company } from "../models/Company.model";
import { CompanyService } from "../shared/company.service";

// Section 2
//We're defining the type of action, which is in the form of a string constant.
export const DELETE_COMPANY = "DELETE_COMPANY";
export const GET_LIST_COMPANY = "GET_LIST_COMPANY";



// Section 3
//We're creating a class for each action with a constructor that allows us to pass in the payload. This isn't a required step, but it does provide you with strong typing.

export class GetListCompany implements Action {
  readonly type = GET_LIST_COMPANY;

  constructor(public payload : Company[]) {}
}

export class DeleteCompany implements Action {
  readonly type = DELETE_COMPANY;

  constructor(public payload: Company[]) { }
}

// Section 4
export type Actions =
  | DeleteCompany
  | GetListCompany;
