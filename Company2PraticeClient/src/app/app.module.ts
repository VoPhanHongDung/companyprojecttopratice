import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CompanyComponent } from './components/company/company.component';
import { MatTableModule,MatButtonModule ,MatButtonToggleModule, MatFormFieldModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CreateCompanyComponent } from './components/create-company/create-company.component';
import { FormsModule } from '@angular/forms';
import { EditCompanyComponent } from './components/edit-company/edit-company.component';
import { CdkTableModule } from '@angular/cdk/table';
import { StoreModule } from '@ngrx/store';
import { reducer } from './reducers/company.reducer';
import { EffectsModule } from '@ngrx/effects';


@NgModule({
  declarations: [
    AppComponent,
    CompanyComponent,
    CreateCompanyComponent,
    EditCompanyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatButtonToggleModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    MatFormFieldModule,
    CdkTableModule,
    StoreModule.forRoot({
      companies: reducer
    }),
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
