﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Company2Pratice.Data;
using Company2Pratice.Repo;

namespace Company2Pratice.Service
{
    public class CompanyService : Repository, ICompanyService
    {
        private IGraphRepository _GraphRepository;

        public CompanyService(IGraphRepository graphRepository) : base(graphRepository)
        {
            this._GraphRepository = graphRepository;
        }


        public async Task CreateCompanyAsync(Company newCom)
        {
            var cypher =  _GraphRepository.GraphClient.Cypher
                .Create(
                    $"(c:{Company.LABEL_COMPANY} {{{nameof(Company.titleCompany)}:$titleCompany,{nameof(Company.nameCompany)}:$nameCompany,{nameof(Company.globalID)}:$globalID,{nameof(Company.dateCreated)}:$dateCreated}})")
                .WithParam("titleCompany", newCom.titleCompany)
                .WithParam("nameCompany", newCom.nameCompany)
                .WithParam("globalID", newCom.globalID)
                .WithParam("dateCreated", newCom.dateCreated)
                .ReturnDistinct(c => c.As<Company>());
            var result = await cypher.ResultsAsync;

        }


        public async Task<Company> GetCompanyByIdAsync(Guid id)
        {
            var query = _GraphRepository.GraphClient.Cypher
            .Match(($"((c:Company))"))
            .Where((Company c) => c.globalID == id)
            .Return(c => c.As<Company>());

            var result = await query.ResultsAsync;
            var companyInfo = result.FirstOrDefault();

            return companyInfo;
        }

        public async Task DeleteCompanyAsync(Guid globalId)
        {
            var query = _GraphRepository.GraphClient.Cypher
            .Match($"(c:{Company.LABEL_COMPANY})")
            .Where((Company c) => c.globalID == globalId)
            .DetachDelete("c");

            await query.ExecuteWithoutResultsAsync();
        }

        public async Task<bool> UpdateCompany(Company comTemp)
        {
           
            if (!string.IsNullOrEmpty(comTemp.globalID.ToString()))
            {
                var cyberQuery = _GraphRepository.GraphClient.Cypher
                    .Match($"(c:{Company.LABEL_COMPANY})")
                    .Where(((Company c) => c.globalID == comTemp.globalID))
                    .Set($"c = {{{nameof(Company.titleCompany)}:$titleCompany,{nameof(Company.nameCompany)}:$nameCompany,{nameof(Company.globalID)}:$globalID,{nameof(Company.dateCreated)}:$dateCreated}}")
                    .WithParam("titleCompany", comTemp.titleCompany)
                    .WithParam("nameCompany", comTemp.nameCompany)
                    .WithParam("dateCreated", comTemp.dateCreated)
                    .WithParam("globalID", comTemp.globalID)
                    .Return(c => c.As<Company>());
                if (cyberQuery.ResultsAsync.IsCompletedSuccessfully)
                {
                    return true;
                }  
            }

            return false;
        }
    }
}
