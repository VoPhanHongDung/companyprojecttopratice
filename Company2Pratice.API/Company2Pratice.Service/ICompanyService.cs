﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Company2Pratice.Data;
using Company2Pratice.Repo;

namespace Company2Pratice.Service
{
    public interface ICompanyService : IRepository
    {
        Task CreateCompanyAsync(Company newCom);
        Task<Company> GetCompanyByIdAsync(Guid id);
        Task DeleteCompanyAsync(Guid globalId);
        Task<bool> UpdateCompany(Company comTemp);
    }
}
