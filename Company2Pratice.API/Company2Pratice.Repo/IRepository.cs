﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Company2Pratice.Data;

namespace Company2Pratice.Repo
{
    public interface IRepository
    {
        Task<IEnumerable<Company>> GetAllCompanyAsync();
    }
}
