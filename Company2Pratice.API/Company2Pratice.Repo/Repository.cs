﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company2Pratice.Data;

namespace Company2Pratice.Repo
{
    public class Repository : IRepository
    {
        private readonly IGraphRepository _graphRepository;

        public Repository(IGraphRepository graphRepository)
        {
            _graphRepository = graphRepository;
        }


        public async Task<IEnumerable<Company>> GetAllCompanyAsync()
        {
            var result = await _graphRepository.GraphClient.Cypher
                .Match($"((c:{Company.LABEL_COMPANY}))")
                .ReturnDistinct(c => c.As<Company>())
                .ResultsAsync;
            return result.ToList();
        }
    }
}
