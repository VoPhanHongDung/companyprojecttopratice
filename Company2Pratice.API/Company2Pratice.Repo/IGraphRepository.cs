﻿using Neo4j.Driver.V1;
using Neo4jClient;

namespace Company2Pratice.Repo
{
    public interface IGraphRepository
    {
        IGraphClient GraphClient { get; set; }
        IDriver Driver { get; set; }
    }
}
