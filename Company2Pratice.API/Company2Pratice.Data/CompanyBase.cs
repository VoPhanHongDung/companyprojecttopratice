﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company2Pratice.Data
{
    public abstract class CompanyBase
    {
        private string lABEL_COMPANY;

        protected CompanyBase(string lABEL_COMPANY)
        {
            this.lABEL_COMPANY = lABEL_COMPANY;
        }

        public Guid globalID { get; set; }
        public DateTime dateCreated { get; set; }
    }
}
