﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company2Pratice.Data
{
    public class Company : CompanyBase
    {
        public const string LABEL_COMPANY = "Company";

        public Company() : base(LABEL_COMPANY)
        {

        }

        public string nameCompany { get; set; }
        public string titleCompany { get; set; }


    }
}
