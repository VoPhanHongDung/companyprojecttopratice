﻿using System;
using Company2Pratice.Repo;
using Company2Pratice.Service;
using Eventful;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Neo4j.Driver.V1;
using Neo4jClient;

namespace Company2Pratice.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Data.Neo4j>(Configuration.GetSection("Neo4j"));
            RegisterNeo4jDriver(services);
            RegisterGraphClient(services);
            services.AddScoped<IGraphRepository, GraphRepository>();
            services.AddScoped<ICompanyService, CompanyService>();
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddCors(options =>
            {
                options.AddPolicy("EnableCORS", builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                        .Build();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseCors("EnableCORS");
            app.UseMvc();
        }

        private void RegisterGraphClient(IServiceCollection services)
        {
            services.AddScoped(typeof(IGraphClient), resolver =>
            {
                var neo4jSetting = Configuration.GetSection("Neo4j").Get<Data.Neo4j>();
                var client = new BoltGraphClient(
                    new Uri(neo4jSetting.Uri),
                    neo4jSetting.User, neo4jSetting.Password);

                client.Connect();

                return client;
            });
        }

        private void RegisterNeo4jDriver(IServiceCollection services)
        {
            services.AddScoped(typeof(IDriver), resolver =>
            {
                var neo4jSetting = Configuration.GetSection("Neo4j").Get<Data.Neo4j>();
                var neo4jDriver = GraphDatabase.Driver(
                    neo4jSetting.Uri,
                    AuthTokens.Basic(neo4jSetting.User, neo4jSetting.Password));

                return neo4jDriver;
            });
        }
    }
}
