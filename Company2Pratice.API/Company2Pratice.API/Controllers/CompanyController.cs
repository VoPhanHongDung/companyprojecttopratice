﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company2Pratice.Data;
using Company2Pratice.Service;
using Microsoft.AspNetCore.Mvc;

namespace Company2Pratice.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class CompanyController : Controller
    {

        private readonly ICompanyService _company;

        public CompanyController(ICompanyService company)
        {
            this._company = company;
        }

        [HttpGet]
        public async Task<IActionResult> GetListCompany()
        {

            try
            {
                var listCompany = await _company.GetAllCompanyAsync();
                return Ok(listCompany);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateCompany([FromBody] Company companyTemp)
        {
            if (string.IsNullOrEmpty(companyTemp.nameCompany) && string.IsNullOrEmpty(companyTemp.titleCompany))
            {
                BadRequest("Object must be not null");
            }
            else
            {
                var newCom = new Company
                {
                    nameCompany = companyTemp.nameCompany,
                    titleCompany = companyTemp.titleCompany,
                    dateCreated = DateTime.Now,
                    globalID = Guid.NewGuid()
                };

                await _company.CreateCompanyAsync(newCom);
                return Ok();
            }
            return NotFound();



        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCompanyById(Guid id)
        {
            try
            {
                var company = await _company.GetCompanyByIdAsync(id);
                return Ok(company);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCompany(Guid id)
        {
            try
            {
                await _company.DeleteCompanyAsync(id);
                return Ok("successful");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCompany(Guid id,[FromBody] Company company)
        {
            var temp = await _company.GetCompanyByIdAsync(id);

            var newCom = new Company
            {
                nameCompany = company.nameCompany,
                titleCompany = company.titleCompany,
                dateCreated = temp.dateCreated,
                globalID = temp.globalID
            };


            bool flag = await _company.UpdateCompany(newCom);
            if (flag) return Ok("successful");
            return BadRequest("Can't update company");
        }
    }
}